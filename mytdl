#!/usr/bin/env ruby
$VERBOSE = true
require "fileutils"
require "find"
require "i18n"
require "json"
require "optparse"
require "pathname"
require "progressbar"
require "yaml"

command_list = ["info", "data", "view"]
USAGE_MESSAGE = "usage: mytdl {%s}" % command_list.join("|")
def die_usage
  puts USAGE_MESSAGE
  exit 100
end
die_usage if ARGV.length != 1
command = ARGV.shift
die_usage if ! command_list.include?(command)
OptionParser.new do |opts|
  opts.program_name = "mytdl"
  opts.version = "0.0.1"
  opts.banner = USAGE_MESSAGE
end.parse!

def lockdown(filename)
  f = File.open(filename, File::CREAT)
  if f.flock(File::LOCK_EX | File::LOCK_NB)
    yield
    f.flock(File::LOCK_UN)
  else
    puts "cannot obtain lock: #{filename}"
  end
  f.close
end

def do_parallel(num_procs, list)
  m = (list.length.to_f / num_procs).ceil
  num_procs.times do |i|
    fork do
      s = i*m
      t = (i+1)*m - 1
      my_list = list[s..t]
      my_list.each { |x| yield x } if my_list
    end
  end
  Process.waitall
end

I18n.config.available_locales = :en
def sanitize(fn)
  return fn if fn.is_a? Numeric
  I18n.transliterate(fn).gsub(/[^0-9A-z.\- ]/, '_')
end

def first_exist(base, *ext_list)
  ext_list.each do |ext|
    fn = "#{base}.#{ext}"
    return fn, ext if File.exist?(fn)
  end
  nil
end

config = YAML.load(File.read("config.yaml"))

case command
when "info"
  urls = config["urls"].map do |x|
    if x.start_with?("https://www.youtube.com/playlist?list=") || x.start_with?("https://www.youtube.com/watch?v=")
      [x]
    else
      [x, "#{x}/playlists"]
    end
  end.flatten
  do_parallel(config["num_procs_info"], urls.shuffle) do |url|
    system(config["youtube_dl"], "-iw",
           "-o", "info/%(id)s/%(playlist_uploader_id)s/%(playlist_id)s",
           "--write-info-json", "--skip-download",
           url)
  end
when "data"
  id_list = Pathname.glob("info/*").map(&:basename).map(&:to_s)
  do_parallel(config["num_procs_data"], id_list.shuffle) do |id|
    next if File.exist?("data/#{id}.done")
    system(config["youtube_dl"], "-f", "bestvideo+bestaudio/best",
           "-o", "data/#{id}.%(ext)s", "https://www.youtube.com/watch?v=#{id}")
    FileUtils.touch("data/#{id}.done") if $?.success?
  end
when "view"
  lockdown(".view.lock") do
    print "collecting: "
    info_list = []
    Find.find("info") do |path|
      info_list << Pathname.new(path) if path.end_with?(".info.json")
      if info_list.length % 1000 == 0
        print "."
        STDOUT.flush
      end
    end
    puts " #{info_list.length}"
    progressbar = ProgressBar.create(:title => "processing", :total => info_list.length)
    new_link_count = 0
    total_link_count = 0
    info_list.each do |info_pn|
      progressbar.increment
      fn = info_pn.to_s
      info = JSON.parse(File.read(fn))
      if ! (info["id"] && info["uploader"] && info["playlist_title"] && info["playlist_index"] && info["upload_date"] && info["title"] && info["ext"])
        puts "WARNING: missing information: #{fn}"
        next
      end
      id = info_pn.dirname.dirname.basename.to_s
      if id != info["id"]
        puts "WARNING: ID mismatch: #{fn}"
        next
      end
      data_file, data_file_ext = first_exist("data/#{id}", info["ext"], "webm", "mkv", "mp4")
      next if data_file.nil?
      uploader_ = sanitize(info["uploader"])
      playlist_title_ = sanitize(info["playlist_title"])
      playlist_index_ = "%05d" % sanitize(info["playlist_index"])
      upload_date_ = sanitize(info["upload_date"])
      title_ = sanitize(info["title"])
      dir = "view/#{uploader_}/#{playlist_title_}"
      ["#{dir}/#{playlist_index_} #{upload_date_} #{title_}.#{data_file_ext}", "#{dir}/#{upload_date_} #{title_}.#{data_file_ext}"].each do |link|
        if ! File.exist?(link)
          FileUtils.mkdir_p(dir)
          FileUtils.ln_s("../../../#{data_file}", link)
          new_link_count += 1
        end
        total_link_count += 1
      end
    end
    percent = (100 * total_link_count.to_f / (2*info_list.length)).round
    puts "number of links: new: #{new_link_count} total: #{total_link_count} (#{percent}%)"
  end
end
puts "mytdl: operations completed!"
